@interface NSString (OTSCategory)

- (NSString*)otsNormalizeString;

@end
